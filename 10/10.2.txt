<!DOCTYPE html>
<html>
    <head>
        <title>10.1</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style 10.2.css">
        </head>
    <body>
        <svg>
            <g>
                <symbol>
                <circle cx="300" cy="150" r="100"/>
                </symbol>
                <text x="300" y="150" text-anchor="middle" fill="url(#Gradient01)">Hello world!</text>
                <path id="1234" d="M 50 50 L 300 300 l -50 -70" stroke="black" stroke-width="4px" fill="none"/>
                <text fill="black">
                    <textPath xlink:href="#1234">изогнутый текст</textPath>
                </text>
                <defs>
                    <linearGradient id="Gradient01">
                        <stop offset="20%" stop-color="green"/>
                        <stop offset="80" stop-color="orange"/>
                    </linearGradient>     
                </defs>
            </g>
        </svg>

    </body>
</html>