var bodyElement = document.querySelector('body');
bodyElement.appendChild(getModal({
    title: '123',
    content: '<button>',
}));
bodyElement.appendChild(getModal({
    title: 'Hello',
    content: '<button>',
}));

function getCloseBtn() {
    var closeBtnElement = document.createElement('button');
    closeBtnElement.style.width = '30px';
    closeBtnElement.style.height = '30px';
    closeBtnElement.style.position = 'absolute';
    closeBtnElement.style.right = '0';
    closeBtnElement.innerHTML = 'X';

    return closeBtnElement;
}

function getModal(options) {
    var modalElement = document.createElement('div');
    var modalHeaderElement = document.createElement('header');
    var modalMainElement = document.createElement('main');
    var modalFooterElement = document.createElement('footer');
    var okBtnElement =  document.createElement('button');
    var cancelBtnElement =  document.createElement('button');
    var spanHeaderElement = document.createElement('span');

    modalElement.appendChild(modalHeaderElement);
    modalElement.appendChild(modalMainElement);
    modalElement.appendChild(modalFooterElement);
    modalFooterElement.appendChild(cancelBtnElement);
    modalFooterElement.appendChild(okBtnElement);
    modalHeaderElement.appendChild(getCloseBtn());
    spanHeaderElement.innerHTML = options.title;
    

    modalHeaderElement.appendChild(spanHeaderElement);


    cancelBtnElement.style.width = '100px';
    cancelBtnElement.style.height = '30px';
    cancelBtnElement.innerHTML = 'CANCEL';
    cancelBtnElement.style.position = 'absolute';
    cancelBtnElement.style.right = '80px';
    cancelBtnElement.style.top = '5px';


    okBtnElement.style.width = '50px';
    okBtnElement.style.height = '30px';
    okBtnElement.innerHTML = 'OK';
    okBtnElement.style.position = 'absolute';
    okBtnElement.style.right = '20px';
    okBtnElement.style.top = '5px';

    modalElement.style.width = '800px';
    modalElement.style.border = '3px solid black';

    modalHeaderElement.style.height = '60px';
    modalHeaderElement.style.position = 'relative';

    modalMainElement.style.height = '100px';
    modalMainElement.style.position = 'relative';
    modalMainElement.innerHTML = options.content;

    modalFooterElement.style.height = '40px';
    modalFooterElement.style.position = 'relative';

    return modalElement;
}
