function log(age, gender) {
    console.log('Возраст: ' + age + ' Пол: ' + gender);
    console.log(this);
}

var logAge = function() {
    console.log(this);
};

console.log(logAge);
log(25, 'male');
log.cat = 'кошка';
console.log(log);
console.dir(log);

(function(age, gender) {
    console.log('Возраст: ' + age + ' Пол: ' + gender);
})(25, 'male');

(function(age, gender) {
    console.log('Возраст: ' + age + ' Пол: ' + gender);
}(25, 'male'));

var f = new logAge(25, 'male');
logAge();