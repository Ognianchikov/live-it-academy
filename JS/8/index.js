var bodyElement = document.querySelector('body');
var divElements = document.querySelectorAll('div');
var boxElement = document.createElement('div');
console.log(divElements);

divElements.forEach(function(item) {
  toolTip(item);
});

function toolTip(elem) {
  elem.addEventListener('mousemove', function(event){
    boxElement.style.display = 'block';
    boxElement.style.top = `${event.pageY - 60}px`;
    boxElement.style.left = `${event.pageX + 10}px`;
    boxElement.innerHTML = elem.dataset.title;
  });
  elem.addEventListener('mouseleave', function(event) {
    boxElement.style.display = 'none';
  });
}

boxElement.style.height = '30px';
boxElement.style.width = '125px';
boxElement.style.border = '1px solid black';
boxElement.style.position = 'absolute';
boxElement.style.display = 'none';
boxElement.style.textAlign = 'center';
boxElement.style.lineHeight = '30px';
boxElement.style.backgroundColor = 'orange';
boxElement.style.color = 'white';
boxElement.style.borderRadius = '10%';
boxElement.style.fontSize = '15px';


bodyElement.appendChild(boxElement);