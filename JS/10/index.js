var box = document.querySelector('.box');
var triangle = document.querySelector('.triangle');
var input = document.querySelector('.input');
var content = document.querySelector('.content');

var fragment = document.createDocumentFragment();

function getListItem(todo) {
    var newDiv = document.createElement('div');
    newDiv.style.textOverflow = 'ellipsis';
    newDiv.style.whiteSpace = 'nowrap';
    newDiv.style.overflow = 'hidden';
    newDiv.innerHTML = todo.id + ' : ' + todo.title;
    return newDiv;
}


box.addEventListener('click', function() {
    if (triangle.style.borderBottom === '0px') {
        triangle.style.borderBottom = '24px solid black';
        triangle.style.borderTop = '0px';
        content.style.display = 'none';
    } else {
        triangle.style.borderTop = '24px solid black';
        triangle.style.borderBottom = '0px';
        content.style.display = 'block';
    }
});

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(json => {
      json.forEach(function(item) {
        fragment.appendChild(getListItem(item));
      });
      content.appendChild(fragment);
  });

input.addEventListener('keypress', function(event) {
    if (event.key === 'Enter') {
        event.preventDefault();
        var textToFind = event.target.innerHTML;
        Array.from(content.children).forEach(function(item) {
            if (item.innerHTML.includes(textToFind)) {
                item.style.display = 'block';
            } else {
                item.style.display = 'none';
            }
        });
    }
});