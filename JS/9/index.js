var active = 0;

function showModal() {
  var modalElementBtn = document.querySelector('#enterinput');
  var modalElementDiv = document.querySelector('#enterdiv');
  var modalElementBcg = document.querySelector('#enterbcg');

  modalElementBtn.style.background = 'white';
  modalElementDiv.style.background = 'rgb(32, 255, 170)';
  modalElementBcg.style.background = 'white';
  if (active === 0) {
    modalElementBtn.style.transform = 'translate(50px, 0)';
    active = 1;
  } else {
    modalElementBtn.style.transform = 'translate(0, 0)';
    modalElementBtn.style.background = 'rgb(32, 255, 170)';
    modalElementDiv.style.background = 'white';
    modalElementBcg.style.background = 'rgb(32, 255, 170)';
    active = 0;
  }
}

var showModalBtn = document.querySelector('#enterinput');
showModalBtn.addEventListener('click', showModal);

