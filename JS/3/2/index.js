var countries = [
    {name: 'Belarus', capital: 'Minsk'},
    {name: 'Russia', capital: 'Moskow'},
    {name: 'Ukraine', capital: 'Kiev'},
    {name: 'Latvia', capital: 'Riga'},
    {name: 'Russia', capital: 'Moskow'}
]

var enterInfoBtn = document.querySelector('#enterInfo');
console.log(enterInfoBtn);

function askCountry() {
    return prompt('Введите название страны');
}

function askCapital() {
    return prompt('Введите название столицы');
}

enterInfoBtn.addEventListener('click', addCountry);

function addCountry() {
    var country = {};
    country.name = askCountry();
    country.capital = askCapital();
    countries.push(country);
    console.log(countries);
}

var showCountriesBtn = document.querySelector('#collection');
console.log(showCountriesBtn);

function showCountries() {
    console.log(countries);
}

showCountriesBtn.addEventListener('click', showCountries);

var countryInfoBtn = document.querySelector('#countryInfo');
console.log(countryInfoBtn);

function getCountryInfo() {
    var countryName = askCountry();
    console.log(countryName);
    var requestCountry = countries.find(function(country) {
        return country.name === countryName;
    })
    console.log(requestCountry);
    if (requestCountry) {
        console.log(requestCountry.capital);
    } else {
        console.log('Нет такой страны');
    }
}

countryInfoBtn.addEventListener('click', getCountryInfo);