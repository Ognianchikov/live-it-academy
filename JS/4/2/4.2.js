var math = {
    PI : 3.14,
    square : function(num) {
        return num**2;
    },

    max: function(a,b) {
        if (a>b) {
            return a;
        } else if (a<b) {
            return b;
        } else {
            return a;
        } 
    },
    maxNative: function(a,b) {
        return Math.max(a,b)
    }

};

console.log(math.maxNative(5,3));

var s = 'Утром деньги';
console.log(s.charAt(4));
